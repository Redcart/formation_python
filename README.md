# Ce dossier s'organise de la maniere suivante:

- Scripts_exercices ==> scripts destines aux TP pour les pratiquants
- Scripts_corriges ==> scripts de correction a envoyer aux pratiquants en fin de formation
- Data ==> donnees necessaires aux TP
- Output ==> Dossier qui regroupera les sorties generes par le code python
